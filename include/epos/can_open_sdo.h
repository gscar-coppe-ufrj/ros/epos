// can_open_sdo.h



#ifndef EPOS_CAN_OPEN_SDO_H
#define EPOS_CAN_OPEN_SDO_H



namespace epos {



class CANopenSDO
{
public:
    unsigned char dataSize;
    unsigned short index;
    unsigned char subIndex;
    unsigned char data[4];

    CANopenSDO & operator=(const CANopenSDO & sdo);
    void ResetData();
};



}



#endif // EPOS_CAN_OPEN_SDO_H

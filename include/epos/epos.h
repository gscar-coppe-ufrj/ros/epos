 // epos.h



#ifndef EPOS_EPOS_H
#define EPOS_EPOS_H



// The max of D/A I/O are the higher number of D/A I/O of all EPOS2 models
// These dimensions have to match info.msg  
#define EPOS_MAX_ANALOG_INPUTS 2
#define EPOS_MAX_DIGITAL_INPUTS 16
#define EPOS_MAX_ANALOG_OUTPUTS 1
#define EPOS_MAX_DIGITAL_OUTPUTS 8

// Drive States
#define EPOS_DRIVE_STATE_MASK 0x417F
#define EPOS_DRIVE_STATE_START 0x0000
#define EPOS_DRIVE_STATE_NOT_READYTO_SWITCH_ON 0x0100
#define EPOS_DRIVE_STATE_SWITCH_ON_DISABLED 0x0140
#define EPOS_DRIVE_STATE_READY_TO_SWITCH_ON 0x0121
#define EPOS_DRIVE_STATE_SWITCHED_ON 0x0123
#define EPOS_DRIVE_STATE_REFRESH 0x4123
#define EPOS_DRIVE_STATE_MEASURE_INIT 0x4133
#define EPOS_DRIVE_STATE_OPERATION_ENABLE 0x0137
#define EPOS_DRIVE_STATE_QUICKSTOP_ACTIVE 0x0117
#define EPOS_DRIVE_STATE_FAULT_REACTION_ACTIVE_DISABLED 0x010F
#define EPOS_DRIVE_STATE_FAULT_REACTION_ACTIVE_ENABLED 0x011F
#define EPOS_DRIVE_STATE_FAULT 0x0108

// Device Control
#define EPOS_DEVICE_CONTROL_CMDS_SHUTDOWN 0x06
#define EPOS_DEVICE_CONTROL_CMDS_SWITCH_ON 0x07
#define EPOS_DEVICE_CONTROL_CMDS_SWITCH_ON_ENABLE_OPEARTION 0x0F
#define EPOS_DEVICE_CONTROL_CMDS_DISABLE_VOLTAGE 0x00
#define EPOS_DEVICE_CONTROL_CMDS_QUICKSTOP 0x02
#define EPOS_DEVICE_CONTROL_CMDS_DISABLE_OPERATION 0x07
#define EPOS_DEVICE_CONTROL_CMDS_ENABLE_OPERATION 0x0F
#define EPOS_DEVICE_CONTROL_CMDS_FAULT_RESET_1 0x00
#define EPOS_DEVICE_CONTROL_CMDS_FAULT_RESET_2 0x80

// Modes of Operation
#define EPOS_MODES_OPERATION_NO_MODE 0 // This has been invented by me (it's not on the EPOS2 manual)
#define EPOS_MODES_OPERATION_INTERPOLATED_POSITION_MODE 7
#define EPOS_MODES_OPERATION_HOMING_MODE 6
#define EPOS_MODES_OPERATION_PROFILE_VELOCITY_MODE 3
#define EPOS_MODES_OPERATION_PROFILE_POSITION_MODE 1
#define EPOS_MODES_OPERATION_POSITION_MODE -1
#define EPOS_MODES_OPERATION_VELOCITY_MODE -2
#define EPOS_MODES_OPERATION_CURRENT_MODE -3
#define EPOS_MODES_OPERATION_DIAGNOSTIC_MODE -4
#define EPOS_MODES_OPERATION_MASTER_ENCODP_EPOS_MODE -5
#define EPOS_MODES_OPERATION_STEP_DIRECTION_MODE -6


// ConfigPDO
//#define EPOS_CONFIG_PDO_GENERAL_PURPOSE_D 12
//#define EPOS_CONFIG_PDO_CONFIGURATION_OF_DIGITAL_INPUTS 0x2070
#define EPOS_CONFIG_PDO_OBJ_CONTROLWORD 0x60400010
#define EPOS_CONFIG_PDO_OBJ_MODES_OF_OPERATION 0x60600008
#define EPOS_CONFIG_PDO_OBJ_DIGITAL_OUTPUT_FUNCTIONALITIES 0x20780110
#define EPOS_CONFIG_PDO_OBJ_CURRENT_MODE_SETTING_VALUE 0x20300010
#define EPOS_CONFIG_PDO_OBJ_TARGET_POSITION 0x607A0020
#define EPOS_CONFIG_PDO_OBJ_TARGET_VELOCITY 0x60FF0020
#define EPOS_CONFIG_PDO_OBJ_STATUSWORD 0x60410010
#define EPOS_CONFIG_PDO_OBJ_MODES_OF_OPERATION_DISPLAY 0x60610008
#define EPOS_CONFIG_PDO_OBJ_CURRENT_ACTUAL_VALUE 0x60780010
#define EPOS_CONFIG_PDO_OBJ_POSITION_ACTUAL_VALUE 0x60640020
#define EPOS_CONFIG_PDO_OBJ_VELOCITY_ACTUAL_VALUE_AVERAGED 0x20280020
#define EPOS_CONFIG_PDO_OBJ_ANALOG_INPUT_1 0x207C0110
#define EPOS_CONFIG_PDO_OBJ_ANALOG_INPUT_2 0x207C0210
#define EPOS_CONFIG_PDO_OBJ_DIGITAL_INPUT_FUNCTIONALITIES_STATE 0x20710110



#include "can_open.h"



namespace epos {



namespace ControlTypes
{
    enum ControlType
    {
        NotControlled,
        SwitchedOn,
        Homing,
        Current,
        Velocity,
        Position
    };
}
typedef ControlTypes::ControlType ControlType;



class Epos : public CANopen
{
    bool faultSentOnce;
    bool homeSentOnce;

protected:
    unsigned short int driveState;
    unsigned short int driveStateSP;

    char operationMode;
    char operationModeSP;

    bool targetReached;
    bool homingAttained;
    bool homingError;

    double position;
    double positionSP;

    double velocity;
    double velocitySP;

    short current;
    short currentSP;

    short analogInput[EPOS_MAX_ANALOG_INPUTS];
    bool digitalInput[EPOS_MAX_DIGITAL_INPUTS];

    short analogOutput[EPOS_MAX_ANALOG_OUTPUTS];
    bool digitalOutput[EPOS_MAX_DIGITAL_OUTPUTS];

    double gainInversion;
    double gainVelocity;
    double gainVeocityInverted;
    double gainPosition;
    double gainPositionInverted;

    bool loaded;
    bool inverted;

    boost::mutex mutUpdate;

    void InitVariables();

    unsigned short GenerateControlWord();

    virtual void LoadSDOs(){}
    bool DeviceConfig();

    virtual void SendingPDO();
    virtual void ReceivingPDO();
    virtual void Emergency(const can_frame &frame) const;

public:
    Epos();
    virtual ~Epos();
};



}



#endif // EPOS_EPOS_H

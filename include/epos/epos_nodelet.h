// epos_nodelet.h



#ifndef EPOS_EPOS_NODELET_H
#define EPOS_EPOS_NODELET_H



#include <ros/ros.h>
#include <nodelet/nodelet.h>
#include <std_msgs/UInt8.h>
#include <std_msgs/Float64.h>
#include <std_msgs/ByteMultiArray.h>
#include <epos/Control.h>
#include <epos/DAOutputs.h>
#include <epos/Connect.h>
#include <epos/Info.h>
#include <logger/log.h>
#include <epos/epos.h>



namespace epos {



class EposNodelet : public TimingLog, public Epos, public nodelet::Nodelet
{
    bool initialized;

    ros::Publisher msgEposStatePub;
    ros::Publisher msgEposInfoPub;
    ros::Publisher msgEposEmergencyPub;
    ros::Publisher msgOutputPub;

    ros::ServiceServer srvEposControlSer;
    ros::ServiceServer srvEposDAOutputsSer;
    ros::ServiceServer srvEposConnectSer;

    bool ControlCallback(epos::Control::Request & req, epos::Control::Response & res);
    bool DAOutputsCallback(epos::DAOutputs::Request & req, epos::DAOutputs::Response & res);
    bool ConnectCallback(epos::Connect::Request & req, epos::Connect::Response & res);

protected:
    void SetState(unsigned char);
    void LoadSDOs();

    virtual void ReceivingPDO();

    virtual void Emergency(const can_frame &frame) const;

public:
    EposNodelet();
    virtual void onInit();
    virtual ~EposNodelet();
};



}



#endif // EPOS_EPOS_NODELET_H

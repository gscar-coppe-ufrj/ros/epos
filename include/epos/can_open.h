// can_open.h



#ifndef EPOS_CAN_OPEN_H
#define EPOS_CAN_OPEN_H



// OVC (On Virtual CAN) comment
#define CAN_OPEN_CAN_DEVICE_NAME "can0"
// and uncomment
//#define CAN_OPEN_CAN_DEVICE_NAME "vcan0"
// /OVC

#define CAN_OPEN_NUMBER_OF_RPDO 4
#define CAN_OPEN_NUMBER_OF_TPDO 4

// NMT States
#define CAN_OPEN_NMT_STATE_BOOT_UP 0x00
#define CAN_OPEN_NMT_STATE_STOPPED 0x04
#define CAN_OPEN_NMT_STATE_OPERACIONAL 0x05
#define CAN_OPEN_NMT_STATE_PRE_OPERACIONAL 0x7F

// NMT Commands
#define CAN_OPEN_NMT_COMMAND_START_REMOTE_NODE 0x01
#define CAN_OPEN_NMT_COMMAND_STOP_REMOTE_NODE 0x02
#define CAN_OPEN_NMT_COMMAND_ENTER_PRE_OPERACIONAL 0x80
#define CAN_OPEN_NMT_COMMAND_RESET_NODE 0x81
#define CAN_OPEN_NMT_COMMAND_RESET_COMMUNICATION 0x82

// SDO Command Specifier
#define CAN_OPEN_CS_READOBJ_SEND 0x40
#define CAN_OPEN_CS_READOBJ_RECV_1BYTE 0x4F
#define CAN_OPEN_CS_READOBJ_RECV_2BYTE 0x4B
#define CAN_OPEN_CS_READOBJ_RECV_4BYTE 0x43
#define CAN_OPEN_CS_WRITEOBJ_SEND_1BYTE 0x2F
#define CAN_OPEN_CS_WRITEOBJ_SEND_2BYTE 0x2B
#define CAN_OPEN_CS_WRITEOBJ_SEND_4BYTE 0x23
#define CAN_OPEN_CS_WRITEOBJ_SEND_BYTESNOTDEFINED 0x22
#define CAN_OPEN_CS_WRITEOBJ_RECV 0x60
#define CAN_OPEN_CS_ABORT 0xC0

// COB-IDs
#define CAN_OPEN_COB_ID_NMT 0x00
#define CAN_OPEN_COB_ID_SYNC 0x80
#define CAN_OPEN_COB_ID_EMERGENCY 0x80
#define CAN_OPEN_COB_ID_TPDO1 0x180
#define CAN_OPEN_COB_ID_RPDO1 0x200
#define CAN_OPEN_COB_ID_TPDO2 0x280
#define CAN_OPEN_COB_ID_RPDO2 0x300
#define CAN_OPEN_COB_ID_TPDO3 0x380
#define CAN_OPEN_COB_ID_RPDO3 0x400
#define CAN_OPEN_COB_ID_TPDO4 0x480
#define CAN_OPEN_COB_ID_RPDO4 0x500
#define CAN_OPEN_COB_ID_TSDO 0x580
#define CAN_OPEN_COB_ID_RSDO 0x600
#define CAN_OPEN_COB_ID_HEARTBEAT 0x700

// Index
#define CAN_OPEN_INDEX_HEARTBEAT_CONSUMER 0x1016
#define CAN_OPEN_INDEX_HEARTBEAT_PRODUCER 0x1017
#define CAN_OPEN_INDEX_RECEIVE_PDO1_PARAMETER 0x1400
#define CAN_OPEN_INDEX_RECEIVE_PDO2_PARAMETER 0x1401
#define CAN_OPEN_INDEX_RECEIVE_PDO3_PARAMETER 0x1402
#define CAN_OPEN_INDEX_RECEIVE_PDO4_PARAMETER 0x1403
#define CAN_OPEN_INDEX_RECEIVE_PDO1_MAPPING 0x1600
#define CAN_OPEN_INDEX_RECEIVE_PDO2_MAPPING 0x1601
#define CAN_OPEN_INDEX_RECEIVE_PDO3_MAPPING 0x1602
#define CAN_OPEN_INDEX_RECEIVE_PDO4_MAPPING 0x1603
#define CAN_OPEN_INDEX_TRANSMIT_PDO1_PARAMTER 0x1800
#define CAN_OPEN_INDEX_TRANSMIT_PDO2_PARAMTER 0x1801
#define CAN_OPEN_INDEX_TRANSMIT_PDO3_PARAMTER 0x1802
#define CAN_OPEN_INDEX_TRANSMIT_PDO4_PARAMTER 0x1803
#define CAN_OPEN_INDEX_TRANSMIT_PDO1_MAPPING 0x1A00
#define CAN_OPEN_INDEX_TRANSMIT_PDO2_MAPPING 0x1A01
#define CAN_OPEN_INDEX_TRANSMIT_PDO3_MAPPING 0x1A02
#define CAN_OPEN_INDEX_TRANSMIT_PDO4_MAPPING 0x1A03

// Subindex
#define CAN_OPEN_SUBINDEX0 0x00
#define CAN_OPEN_SUBINDEX1 0x01
#define CAN_OPEN_SUBINDEX2 0x02
#define CAN_OPEN_SUBINDEX3 0x03
#define CAN_OPEN_SUBINDEX4 0x04
#define CAN_OPEN_SUBINDEX5 0x05
#define CAN_OPEN_SUBINDEX6 0x06
#define CAN_OPEN_SUBINDEX7 0x07
#define CAN_OPEN_SUBINDEX8 0x08

// Heartbeat
// OVC (On Virtual CAN) comment
#define CAN_OPEN_HEARTBEAT_CONSUMER_TIME 1200
#define CAN_OPEN_HEARTBEAT_PRODUCER_TIME 1000
// and uncomment
//#define CAN_OPEN_HEARTBEAT_CONSUMER_TIME 6000
//#define CAN_OPEN_HEARTBEAT_PRODUCER_TIME 5000
// /OVC

// Emergency
#define CAN_OPEN_EMERGENCY_MAX_WAIT_TIME_SEC 1
#define CAN_OPEN_EMERGENCY_MAX_WAIT_TIME_USEC 0




#include <net/if.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/ioctl.h>
#include <linux/can.h>
#include <linux/can/raw.h>
#include <boost/thread.hpp>
#include <boost/lexical_cast.hpp>
#include <atomic>
#include <bitset>

#include "can_open_sdo.h"



namespace epos {



namespace CanOpenStates {
    enum CanOpenState
    {
        Unknown,
        NotConnected,
        Connecting,
        Disconnecting,
        Connected
    };
}
typedef CanOpenStates::CanOpenState CanOpenState;



typedef boost::posix_time::ptime Time;
typedef boost::posix_time::time_duration TimeDuration;



class CANopen
{
    // Variables unrelated to the CANopen device (are related to how threads work)

    unsigned char state;

    bool heartBeatOK;
    std::atomic<bool> disconnect;

    unsigned int nmtStartWaitToRecv;

    TimeDuration pdoWaitToRecv;
    TimeDuration heartSendPeriod;
    TimeDuration heartRecvPeriod;

    boost::thread* threadConnect;
    boost::thread* threadDisconnect;
    boost::thread* threadPDO;
    boost::thread* threadHeartbeatSend;
    boost::thread* threadHeartbeatRecv;
    boost::thread* threadEmergency;

    boost::mutex mutState;


    // Variables related to the CANopen device

    int socketNMT;
    int socketSDO;
    int socketSync;
    int socketPDO;
    int socketHeartbeat;
    int socketEmergency;

    can_frame msgNMT;
    can_frame msgSync;
    can_frame msgHeartbeatSend;
    can_frame msgHeartbeatRecv;
    can_frame msgEmergency;

    unsigned int nmtWriteErrorCounter;
    unsigned int nmtReadErrorCounter;
    unsigned int sdoWriteWriteErrorCounter;
    unsigned int sdoWriteReadErrorCounter;
    unsigned int sdoReadWriteErrorCounter;
    unsigned int sdoReadReadErrorCounter;
    unsigned int syncErrorCounter;
    unsigned int heartSendErrorCounter;
    unsigned int heartRecvErrorCounter;

    int CreateSocket(can_filter* rfilter, size_t sizerfilter);
    void CreateSockets();
    void DestroySockets();

    void ConnectThread();
    void DisconnectThread();
    void PDOThread();
    void HeartBeatSendThread();
    void HeartBeatRecvThread();
    void EmergencyThread();

protected:
    std::string canDevice;

    std::atomic<bool> tryToReconnect;

    unsigned char deviceNMTState;

    // Be aware that the 'r' is used for messages EPOS receive, and 't' for messages EPOS transmit
    // This variables indicate if the pdos should be sent or received
    bool rPDO[CAN_OPEN_NUMBER_OF_RPDO];
    bool tPDO[CAN_OPEN_NUMBER_OF_TPDO];

    // This variables indicate if the pdos were sent or received
    bool rPDOSent[CAN_OPEN_NUMBER_OF_RPDO];
    bool tPDOReceived[CAN_OPEN_NUMBER_OF_TPDO];

    // This varialbes are buffers for pdos to be sent or recieved
    can_frame frameRPDO[CAN_OPEN_NUMBER_OF_RPDO];
    can_frame frameTPDO[CAN_OPEN_NUMBER_OF_TPDO];

    unsigned char deviceNodeID;
    unsigned char classNodeID;

    std::vector<CANopenSDO> deviceConfigFrames;

    virtual void InitVariables() = 0;

    void MountFrames();

    virtual void SetState(unsigned char state);
    unsigned char GetState();

    virtual void SetWaitToRecvPeriod(TimeDuration time);
    virtual void SetHeartBeatSendPeriod(TimeDuration time);
    virtual void SetHeartBeatRecvPeriod(TimeDuration time);

    virtual bool DeviceConfig();
    virtual bool ConnectWithDevice();

    void Connect();
    void Disconnect(bool wait = false);

    bool SendNMT(unsigned char cmd, unsigned int sleeptimems = 0);
    bool ReadSDO(CANopenSDO* sdo);
    bool WriteSDO(CANopenSDO* sdo);

    virtual void SendingPDO() = 0;
    virtual void ReceivingPDO() = 0;
    virtual void Emergency(const can_frame &frame) const = 0;

public:
    CANopen();

    static void PrintHex(std::ostream &stream, unsigned int number, unsigned int width = 2);

    template <unsigned int nbits>
    static void PrintBin(std::ostream &stream, unsigned int number)
    {
        std::bitset<nbits> binary(number);
        stream << binary;

    }

    virtual ~CANopen();
};



}



#endif // EPOS_CAN_OPEN_H

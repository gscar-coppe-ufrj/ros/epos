// can_open.cpp



#include <epos/can_open.h>
#include <iostream>



epos::CANopen::CANopen()
{
    deviceNodeID = 0;

    state = CanOpenState::NotConnected;

    canDevice = CAN_OPEN_CAN_DEVICE_NAME;
    tryToReconnect = true;

    heartBeatOK = false;
    disconnect = false;

    socketNMT = 0;
    socketSDO = 0;
    socketSync = 0;
    socketPDO = 0;
    socketHeartbeat = 0;
    socketEmergency = 0;

    threadConnect = NULL;
    threadDisconnect = NULL;
    threadPDO = NULL;
    threadHeartbeatSend = NULL;
    threadHeartbeatRecv = NULL;
    threadEmergency = NULL;

    deviceNMTState = CAN_OPEN_NMT_STATE_BOOT_UP;

    for (unsigned char i = 0; i < CAN_OPEN_NUMBER_OF_RPDO; i++)
        rPDO[i] = false;
    for (unsigned char i = 0; i < CAN_OPEN_NUMBER_OF_TPDO; i++)
        tPDO[i] = false;

    // OVC (On Virtual CAN) comment
    nmtStartWaitToRecv = CAN_OPEN_HEARTBEAT_CONSUMER_TIME;
    // and uncomment
    //nmtwaittorecv = 10;
    // /OVC
    SetWaitToRecvPeriod(boost::posix_time::milliseconds(5));
    SetHeartBeatSendPeriod(boost::posix_time::milliseconds(CAN_OPEN_HEARTBEAT_PRODUCER_TIME));
    SetHeartBeatRecvPeriod(boost::posix_time::milliseconds(CAN_OPEN_HEARTBEAT_CONSUMER_TIME));

    nmtWriteErrorCounter = 0;
    nmtReadErrorCounter = 0;
    sdoWriteWriteErrorCounter = 0;
    sdoWriteReadErrorCounter = 0;
    sdoReadWriteErrorCounter = 0;
    sdoReadReadErrorCounter = 0;
    syncErrorCounter = 0;
    heartSendErrorCounter = 0;
    heartRecvErrorCounter = 0;
}


void epos::CANopen::PrintHex(std::ostream &stream, unsigned int number, unsigned int width)
{
    // Save stream state
    std::ios::fmtflags flags(stream.flags());

    // Print
    stream << std::hex << std::setfill('0') << std::setw(width) << number;

    // Restore
    stream.flags(flags);
}


void epos::CANopen::MountFrames()
{
    msgNMT.can_id = CAN_OPEN_COB_ID_NMT;
    msgNMT.can_dlc = 2;
    msgNMT.data[1] = deviceNodeID;

    msgHeartbeatSend.can_id = CAN_OPEN_COB_ID_HEARTBEAT + classNodeID;
    msgHeartbeatSend.can_dlc = 1;
    msgHeartbeatSend.data[0] = CAN_OPEN_NMT_STATE_OPERACIONAL;

    frameRPDO[0].can_id = CAN_OPEN_COB_ID_RPDO1 + deviceNodeID;
    frameRPDO[1].can_id = CAN_OPEN_COB_ID_RPDO2 + deviceNodeID;
    frameRPDO[2].can_id = CAN_OPEN_COB_ID_RPDO3 + deviceNodeID;
    frameRPDO[3].can_id = CAN_OPEN_COB_ID_RPDO4 + deviceNodeID;
}


void epos::CANopen::SetState(unsigned char state)
{
    this->state = state;
}


unsigned char epos::CANopen::GetState()
{
    return state;
}


void epos::CANopen::SetWaitToRecvPeriod(TimeDuration time)
{
    pdoWaitToRecv = time;
}


void epos::CANopen::SetHeartBeatSendPeriod(TimeDuration time)
{
    heartSendPeriod = time;
}


void epos::CANopen::SetHeartBeatRecvPeriod(TimeDuration time)
{
    heartRecvPeriod = time;

    if (socketHeartbeat != 0)
    {
        struct timeval tv;
        tv.tv_sec = heartRecvPeriod.total_seconds();
        tv.tv_usec = heartRecvPeriod.total_microseconds()%1000000;
        setsockopt(socketHeartbeat, SOL_SOCKET, SO_RCVTIMEO, (char*) &tv, sizeof(struct timeval));
    }
}


bool epos::CANopen::DeviceConfig()
{
    bool ret = true;

    for (std::vector<CANopenSDO>::iterator it = deviceConfigFrames.begin(); it != deviceConfigFrames.end(); it++)
    {
        if (!WriteSDO(&(*it)))
        {
            // OVC (On Virtual CAN) comment
            ret = false;
            break;
            // /OVC
        }

        // Sleep for a little while before sending another SDO to avoid
        // flooding the CAN network
        boost::this_thread::sleep(boost::posix_time::milliseconds(2));
    }

    return ret;
}


bool epos::CANopen::ConnectWithDevice()
{
    bool ret = SendNMT(CAN_OPEN_NMT_COMMAND_RESET_COMMUNICATION, 1000);

    //if (ret)
        //ret = SendNMT(CAN_OPEN_NMT_COMMAND_RESET_NODE, 1000); // clear interal errors??

    if (ret)
        ret = DeviceConfig();

    if (ret)
        ret = SendNMT(CAN_OPEN_NMT_COMMAND_START_REMOTE_NODE, nmtStartWaitToRecv);

    return ret;
}


int epos::CANopen::CreateSocket(can_filter* rfilter, size_t sizerfilter)
{
    int s;
    if ((s = socket(PF_CAN, SOCK_RAW, CAN_RAW)) < 0)
        std::cerr << "[ERROR] socket create error | id=" << (int)deviceNodeID << std::endl;

    struct sockaddr_can addr;
    struct ifreq ifr;

    addr.can_family = AF_CAN;
    strcpy(ifr.ifr_name, canDevice.c_str());
    ioctl(s, SIOCGIFINDEX, &ifr);
    addr.can_ifindex = ifr.ifr_ifindex;

    setsockopt(s, SOL_CAN_RAW, CAN_RAW_FILTER, rfilter, sizerfilter);

    if (::bind(s, (struct sockaddr *)&addr, sizeof(addr)) < 0)
        std::cerr << "[ERROR] socket bind error | id=" << (int)deviceNodeID << std::endl;

    return s;
}


void epos::CANopen::CreateSockets()
{
    can_filter rfilternmt;
    // The sockenmt will be used to get the nmt state after a nmt is sent, and this data will come from an heartbeat COB-ID
    // The difference is that the socketnmt will wait 1us on the read(), while the socketheartbeat could wait for seconds
    rfilternmt.can_id   = CAN_OPEN_COB_ID_HEARTBEAT + deviceNodeID;
    rfilternmt.can_mask = CAN_SFF_MASK;

    can_filter rfiltersdo;
    rfiltersdo.can_id   = CAN_OPEN_COB_ID_TSDO + deviceNodeID;
    rfiltersdo.can_mask = CAN_SFF_MASK;

    can_filter rfiltersync;
    rfiltersync.can_id   = CAN_OPEN_COB_ID_SYNC;
    rfiltersync.can_mask = CAN_SFF_MASK;

    can_filter rfilterpdo[CAN_OPEN_NUMBER_OF_TPDO];
    rfilterpdo[0].can_id   = CAN_OPEN_COB_ID_TPDO1 + deviceNodeID;
    rfilterpdo[0].can_mask = CAN_SFF_MASK;
    rfilterpdo[1].can_id   = CAN_OPEN_COB_ID_TPDO2 + deviceNodeID;
    rfilterpdo[1].can_mask = CAN_SFF_MASK;
    rfilterpdo[2].can_id   = CAN_OPEN_COB_ID_TPDO3 + deviceNodeID;
    rfilterpdo[2].can_mask = CAN_SFF_MASK;
    rfilterpdo[3].can_id   = CAN_OPEN_COB_ID_TPDO4 + deviceNodeID;
    rfilterpdo[3].can_mask = CAN_SFF_MASK;

    can_filter rfilterheartbeat;
    rfilterheartbeat.can_id   = CAN_OPEN_COB_ID_HEARTBEAT + deviceNodeID;
    rfilterheartbeat.can_mask = CAN_SFF_MASK;

    can_filter rfilteremergency;
    rfilteremergency.can_id   = CAN_OPEN_COB_ID_EMERGENCY + deviceNodeID;
    rfilteremergency.can_mask = CAN_SFF_MASK;

    socketNMT = CreateSocket(&rfilternmt, sizeof(rfilternmt));
    socketSDO = CreateSocket(&rfiltersdo, sizeof(rfiltersdo));
    socketSync = CreateSocket(&rfiltersync, sizeof(rfiltersync));
    socketPDO = CreateSocket((can_filter*)&rfilterpdo, sizeof(rfilterpdo));
    socketHeartbeat = CreateSocket(&rfilterheartbeat, sizeof(rfilterheartbeat));
    socketEmergency = CreateSocket(&rfilteremergency, sizeof(rfilteremergency));


    struct timeval tv;

    tv.tv_sec = 0;
    tv.tv_usec = 1;
    setsockopt(socketNMT, SOL_SOCKET, SO_RCVTIMEO, (char*) &tv, sizeof(struct timeval));

    tv.tv_usec = 30000; // SDO can't hold forever
    setsockopt(socketSDO, SOL_SOCKET, SO_RCVTIMEO, (char*) &tv, sizeof(struct timeval));

    tv.tv_usec = 1;
    setsockopt(socketPDO, SOL_SOCKET, SO_RCVTIMEO, (char*) &tv, sizeof(struct timeval));

    SetHeartBeatRecvPeriod(heartRecvPeriod);

    tv.tv_sec = CAN_OPEN_EMERGENCY_MAX_WAIT_TIME_SEC;
    tv.tv_usec = CAN_OPEN_EMERGENCY_MAX_WAIT_TIME_USEC;
    setsockopt(socketEmergency, SOL_SOCKET, SO_RCVTIMEO, (char*) &tv, sizeof(struct timeval));
}


void epos::CANopen::DestroySockets()
{
    std::cout << "[ INFO] Destroying sockets from device id = " << (int)deviceNodeID << std::endl;
    close(socketNMT);
    close(socketSDO);
    close(socketSync);
    close(socketPDO);
    close(socketEmergency);
    close(socketHeartbeat);
}


void epos::CANopen::ConnectThread()
{
    std::cout << "[ INFO] Connecting to device id = " << (int)deviceNodeID << "..." << std::endl;

    CreateSockets();

    bool ok = false;

    bool sleep;

    while (!ok && !disconnect)
    {
        ok = ConnectWithDevice();

        if (ok && !disconnect)
        {
            mutState.lock();
            InitVariables();

            sleep = false;
            threadPDO = new boost::thread(boost::bind(&CANopen::PDOThread, this));
            threadHeartbeatSend = new boost::thread(boost::bind(&CANopen::HeartBeatSendThread, this));
            threadHeartbeatRecv = new boost::thread(boost::bind(&CANopen::HeartBeatRecvThread, this));
            threadEmergency = new boost::thread(boost::bind(&CANopen::EmergencyThread, this));
            SetState(CanOpenState::Connected);
            mutState.unlock();
        }
        else
        {
            std::cerr << "[ERROR] Connection failed - device id = " << (int)deviceNodeID << std::endl;
            sleep = !disconnect;
        }

        if (sleep)
            boost::this_thread::sleep(boost::posix_time::milliseconds(500));
    }

    threadConnect = NULL;

    std::cout << "[ INFO] Connected to device id = " << (int)deviceNodeID << std::endl;
}


void epos::CANopen::DisconnectThread()
{
    std::cout << "[ INFO] Disonnecting from device id = " << (int)deviceNodeID << std::endl;

    mutState.lock();

    disconnect = true;

    if (threadConnect != NULL)
    {
        threadConnect->join();
        delete threadConnect;
        threadConnect = NULL;
    }
    if (threadPDO != NULL)
    {
        threadPDO->join();
        delete threadPDO;
        threadPDO = NULL;
    }
    if (threadHeartbeatSend != NULL)
    {
        threadHeartbeatSend->join();
        delete threadHeartbeatSend;
        threadHeartbeatSend = NULL;
    }
    if (threadHeartbeatRecv != NULL)
    {
        threadHeartbeatRecv->join();
        delete threadHeartbeatRecv;
        threadHeartbeatRecv = NULL;
    }
    if (threadEmergency != NULL)
    {
        threadEmergency->join();
        delete threadEmergency;
        threadEmergency = NULL;
    }

    DestroySockets();

    SetState(CanOpenState::NotConnected);

    disconnect = false;

    mutState.unlock();

    if (tryToReconnect)
        Connect();

    std::cout << "[ INFO] Disconnected from device id = " << (int)deviceNodeID << std::endl;
}


void epos::CANopen::Connect()
{
    mutState.lock();
    if (GetState() == CanOpenState::NotConnected && threadConnect == NULL)
    {
        SetState(CanOpenState::Connecting);
        threadConnect = new boost::thread(boost::bind(&CANopen::ConnectThread, this));
    }
    mutState.unlock();
}


void epos::CANopen::Disconnect(bool wait)
{
    mutState.lock();
    if (GetState() == CanOpenState::Connected || GetState() == CanOpenState::Connecting && threadDisconnect == NULL)
    {
        SetState(CanOpenState::Disconnecting);
        threadDisconnect = new boost::thread(boost::bind(&CANopen::DisconnectThread, this));
    }
    mutState.unlock();

    if (wait)
        threadDisconnect->join();
}


bool epos::CANopen::SendNMT(unsigned char cmd, unsigned int sleeptimems)
{
    // first the socketnmt must be empty (he reads the same things as the heartbeat)

    can_frame frame;

    while (read(socketNMT, &frame, sizeof(struct can_frame)) >= 0)
    {}

    // now we can proceed

    msgNMT.can_dlc = 2;
    msgNMT.data[0] = cmd;
    ssize_t w = write(socketNMT, &msgNMT, sizeof(msgNMT));
    if (w < 0)
    {
        std::cerr << "[ERROR] Can't Write NMT | nodeid=" << (int)deviceNodeID << " cmd=" << (int)cmd << " counter=" << nmtWriteErrorCounter++ << " writerror=" << w << std::endl;
        return false;
    }
    else
    {
        /*// OVC (On Virtual CAN) uncomment
        frame.can_dlc = 1;
        frame.can_id = CAN_OPEN_COB_ID_HEARTBEAT + devicenodeid;
        if (cmd == CAN_OPEN_NMT_COMMAND_RESET_NODE)  //cmd was used to Start Remot Mode
            frame.data[0] = CAN_OPEN_NMT_STATE_BOOT_UP;
        else if (cmd == CAN_OPEN_NMT_COMMAND_START_REMOTE_NODE)  //cmd was used to Start Remot Mode
            frame.data[0] = CAN_OPEN_NMT_STATE_OPERACIONAL;
        if (write(socketheartbeat, &frame, sizeof(frame)) < 0)
            cout << "NMT couldn`t be sent" << endl;
        */ // OVC

        boost::this_thread::sleep(boost::posix_time::milliseconds(sleeptimems));

        // check if the heartbeat message with the current nmt state was received
        if (cmd == CAN_OPEN_NMT_COMMAND_RESET_NODE)  //cmd was used to Start Remot Mode
        {
            ssize_t r;
            while (true)
            {
                r = read(socketNMT, &frame, sizeof(struct can_frame));
                if (r < 0)
                    break;
                if (frame.can_id == (CAN_OPEN_COB_ID_HEARTBEAT + deviceNodeID) && (frame.data[0] == CAN_OPEN_NMT_STATE_BOOT_UP || frame.data[0] == 2))
                {
                    deviceNMTState = CAN_OPEN_NMT_STATE_BOOT_UP;
                    return true;
                }
            }
            std::cerr << "[ERROR] Couldn't read NMT Command Reset Node | nodeid=" << (int)deviceNodeID << " counter=" << nmtReadErrorCounter++ << " readrror=" << r << std::endl;
            return false;
        }
        else if (cmd == CAN_OPEN_NMT_COMMAND_START_REMOTE_NODE)  //cmd was used to Start Remot Mode
        {
            ssize_t r;
            while (true)
            {
                r = read(socketNMT, &frame, sizeof(struct can_frame));
                if (r < 0)
                    break;
                if (frame.can_id == (CAN_OPEN_COB_ID_HEARTBEAT + deviceNodeID) && frame.data[0] == CAN_OPEN_NMT_STATE_OPERACIONAL)
                {
                    deviceNMTState = frame.data[0];
                    return true;
                }
            }
            std::cerr << "Couldn't read NMT Command Start Remote Node | nodeid=" << (int)deviceNodeID << " counter=" << nmtReadErrorCounter++ << " readrror=" << r << std::endl;
            return false;
        }
        else
            return true;
    }
}


bool epos::CANopen::ReadSDO(CANopenSDO* sdo)
{
    // The sdo.datasize indicates the expected number of bytes should be read, and if it doesn't happen the func returns false


    can_frame frame;
    frame.can_id = CAN_OPEN_COB_ID_RSDO + deviceNodeID;

    frame.data[0] = CAN_OPEN_CS_READOBJ_SEND;
    frame.data[1] = sdo->index&0xFF;
    frame.data[2] = sdo->index >> 8;
    frame.data[3] = sdo->subIndex;

    ssize_t w = write(socketSDO, &frame, sizeof(frame));
    if (w == sizeof(struct can_frame))
    {
        ssize_t r = read(socketSDO, &frame, sizeof(struct can_frame));
        if (r == sizeof(struct can_frame)) // maybe something need to be done with the frame.can_dlc instead of the return of read()
        {
            if (frame.data[0] == CAN_OPEN_CS_ABORT)
            {
                std::cerr << "[ERROR] SDO write abort | nodeid=" << (int)deviceNodeID << " frame: " << std::hex << frame.can_id << "#";
                for (unsigned char i = 0; i < frame.can_dlc; i++)
                    std::cerr << std::hex << (unsigned int)frame.data[i] << ".";
                std::cerr << std::dec << std::endl;
                return false;
            }
            else
            {
                if (frame.data[0] == CAN_OPEN_CS_READOBJ_RECV_1BYTE)
                    sdo->dataSize = 1;
                else if (frame.data[0] == CAN_OPEN_CS_READOBJ_RECV_2BYTE)
                    sdo->dataSize = 2;
                for (unsigned char i = 0; i < sdo->dataSize; i++)
                    sdo->data[i] = frame.data[4 + i];

                return true;
            }
        }
        else
        {
            std::cerr << "[ERROR] Can't read on Read SDO | nodeid=" << (int)deviceNodeID << " counter=" << sdoReadReadErrorCounter++ << " readrror=" << r << std::endl;
            return false;
        }
    }
    else
    {
        std::cerr << "[ERROR] Can't write on Read SDO | nodeid=" << (int)deviceNodeID << " counter=" << sdoReadWriteErrorCounter++ << " writerror=" << w << std::endl;
        return false;
    }
}


bool epos::CANopen::WriteSDO(CANopenSDO* sdo)
{
    // The sdo.datasize indicates the number of bytes to be written. When the functions returns, sdo.datasize is 4 (normal ReadSDO return size)

    can_frame frame;
    frame.can_id = CAN_OPEN_COB_ID_RSDO + deviceNodeID;

    if (sdo->dataSize > 4)
        return false;
    frame.can_dlc = 4 + sdo->dataSize;

    frame.data[0] = CAN_OPEN_CS_WRITEOBJ_SEND_BYTESNOTDEFINED; // if this always work, why bother to choose the correct size?
    frame.data[1] = sdo->index&0xFF;
    frame.data[2] = sdo->index >> 8;
    frame.data[3] = sdo->subIndex;
    for (unsigned char i = 0; i < sdo->dataSize; i++)
        frame.data[4 + i] = sdo->data[i];

    ssize_t w = write(socketSDO, &frame, sizeof(struct can_frame));
    if (w == sizeof(struct can_frame))
    {
        ssize_t r = read(socketSDO, &frame, sizeof(struct can_frame));
        if (r == sizeof(struct can_frame))
        {
            for (unsigned char i = 0; i < sdo->dataSize; i++)
                sdo->data[i] = frame.data[4 + i];
            sdo->dataSize = 4;

            if (frame.data[0] == CAN_OPEN_CS_ABORT)
            {
                std::cerr << "[ERROR] SDO write abort: " << std::hex << frame.can_id << "#";
                for (unsigned char i = 0; i < frame.can_dlc; i++)
                    std::cerr << std::hex << (unsigned int)frame.data[i] << ".";
                std::cerr << std::dec << std::endl;
                return false;
            }
            else
                return true;
        }
        else
        {
            std::cerr << "[ERROR] Can't read on Write SDO | nodeid = " << (int)deviceNodeID << " counter = " << sdoWriteReadErrorCounter++ << " readrror = " << r << std::endl;
            return false;
        }
    }
    else
    {
        std::cerr << "[ERROR] Can't write on Write SDO | nodeid = " << (int)deviceNodeID << " counter = " << sdoWriteWriteErrorCounter++ << " writerror = " << w << std::endl;
        return false;
    }
}


void epos::CANopen::PDOThread()
{
    struct timeval tv;
    tv.tv_sec = 0;
    tv.tv_usec = 1;
    setsockopt(socketSync, SOL_SOCKET, SO_RCVTIMEO, (char*) &tv, sizeof(struct timeval));

    while (read(socketSync, &msgSync, sizeof(struct can_frame)) >= 0)
    {}

    tv.tv_sec = 1; // when set to 0, the read was blocking forever even when closing it
    tv.tv_usec = 0;
    setsockopt(socketSync, SOL_SOCKET, SO_RCVTIMEO, (char*) &tv, sizeof(struct timeval));

    while (!disconnect)
    {
        ssize_t r = read(socketSync, &msgSync, sizeof(struct can_frame));
        if (r < 0)
            std::cerr << "[ERROR] Sync receive error | nodeid = " << (int) deviceNodeID << " counter = " << syncErrorCounter++ << " readerror = " << r << std::endl;
        else
        {
            SendingPDO();   // in epos.c, depends on epos configuration

            for (unsigned char i = 0; i < CAN_OPEN_NUMBER_OF_RPDO; i++)
            {
                if (rPDO[i])  // if true send PDO (rPDO[i]) to EPOS
                    rPDOSent[i] = (write(socketPDO, &(frameRPDO[i]), sizeof(struct can_frame)) < 0);
            }

            boost::this_thread::sleep(pdoWaitToRecv);  // Waiting before reading PDO from EPOS

            can_frame frame;
            int i;
            // read PDOs (tPDO[i]) from EPOS
            while (read(socketPDO, &frame, sizeof(struct can_frame)) >= 0)
            {
                i=-1;
                if (frame.can_id == (CAN_OPEN_COB_ID_TPDO1 + deviceNodeID))
                    i = 0;
                else if (frame.can_id == (CAN_OPEN_COB_ID_TPDO2 + deviceNodeID))
                    i = 1;
                else if (frame.can_id == (CAN_OPEN_COB_ID_TPDO3 + deviceNodeID))
                    i = 2;
                else if (frame.can_id == (CAN_OPEN_COB_ID_TPDO4 + deviceNodeID))
                    i = 3;
                if (i >= 0 && i < CAN_OPEN_NUMBER_OF_TPDO)
                {
                    tPDOReceived[i] = true;
                    if (tPDO[i])
                    {
                        frameTPDO[i].can_dlc = frame.can_dlc;
                        for (unsigned char j = 0; j < 8; j++)
                            frameTPDO[i].data[j] = frame.data[j];
                    }
                }
            }

            // Check if a Sync is detected but  a tPDO is not received - EPOS bug?? detected in Doris manipulator
            // Problem (potential): too many rPDOs sent to EPOS, taking to much time, leaving no time to EPOS send tPDO
            //  New epos(4)  has a time window parameter.
            // Solution for Doris manipulator:  reducing number of rPDO, only rPDO[0-1].
            // Solution (more general): only send rPDO if there is change in the variable.
            if (!tPDOReceived[1])
                std::cout << "error PDO 1: " << (int) deviceNodeID << " - " << tPDOReceived[0] << tPDOReceived[1] << tPDOReceived[2] << std::endl;

            ReceivingPDO();   // in epos.c, depends on epos configuration

            for (unsigned char i = 0; i < CAN_OPEN_NUMBER_OF_TPDO; i++)
                tPDOReceived[i] = false;
        }
    }
}


void epos::CANopen::HeartBeatSendThread()
{
    while (!disconnect)
    {
        ssize_t w = write(socketHeartbeat, &msgHeartbeatSend, sizeof(struct can_frame));
        if (w < 0)
            std::cerr << "[ERROR] Heartbeat send error | nodeid=" << (int)deviceNodeID << " counter=" << heartSendErrorCounter++ << " writeerror=" << w << std::endl;

        boost::this_thread::sleep(heartSendPeriod);
    }
}


void epos::CANopen::HeartBeatRecvThread()
{
    bool ok = true;

    while (!disconnect && ok)
    {
        ssize_t r = read(socketHeartbeat, &msgHeartbeatRecv, sizeof(struct can_frame));
        if (r < 0)
        {
            // OVC (On Virtual CAN) comment
            std::cerr << "[ERROR] Heartbeat receive error | nodeid=" << (int)deviceNodeID << " counter=" << heartRecvErrorCounter++ << " readerror=" << r << std::endl;

            Disconnect();
            ok = false;
            // /OVC
        }
        else
            deviceNMTState = msgHeartbeatRecv.data[0];
    }
}


void epos::CANopen::EmergencyThread()
{
    while (!disconnect)
    {
        if (read(socketEmergency, &msgEmergency, sizeof(struct can_frame)) < 0)
        {}
        else
            Emergency(msgEmergency); // this function will use the msgemergency data to check and then treat the error
    }
}


epos::CANopen::~CANopen()
{
    if (threadDisconnect != NULL)
        delete threadDisconnect;
}


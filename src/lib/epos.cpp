// epos.cpp



#include <epos/epos.h>
#include <iostream>


epos::Epos::Epos() :
    faultSentOnce(false),
    homeSentOnce(false),
    targetReached(false),
    homingAttained(false),
    homingError(false),
    //
    gainInversion(1),
    gainVelocity(1),
    gainVeocityInverted(1),
    gainPosition(1),
    gainPositionInverted(1),
    //
    loaded(false),
    inverted(false)
{
    rPDO[0] = true;
    rPDO[1] = true;
    rPDO[2] = true;   // In DORIS is set to false due to detected communication error: this PDO is not used in DORIS.
    rPDO[3] = false;
    tPDO[0] = true;
    tPDO[1] = true;
    tPDO[2] = true;
    tPDO[3] = false;

    InitVariables();

    unsigned char i;
    for (i = 0; i < EPOS_MAX_ANALOG_INPUTS; i++)
        analogInput[i] = 0;
    for (i = 0; i < EPOS_MAX_DIGITAL_INPUTS; i++)
        digitalInput[i] = 0;

    for (i = 0; i < EPOS_MAX_ANALOG_OUTPUTS; i++)
        analogOutput[i] = 0;
    for (i = 0; i < EPOS_MAX_DIGITAL_OUTPUTS; i++)
        digitalOutput[i] = 0;
}


void epos::Epos::InitVariables()
{
    operationMode = EPOS_DRIVE_STATE_START;
    operationModeSP = EPOS_DRIVE_STATE_START;

    position = 0;
    positionSP = 0;

    velocity = 0;
    velocitySP = 0;

    current = 0;
    currentSP = 0;
}


unsigned short epos::Epos::GenerateControlWord()
{
    unsigned short controlword = 0;

    if (driveState == EPOS_DRIVE_STATE_FAULT)
    {
        if (faultSentOnce)
        {
            controlword = EPOS_DEVICE_CONTROL_CMDS_FAULT_RESET_2;
            faultSentOnce = false;
        }
        else
        {
            controlword = EPOS_DEVICE_CONTROL_CMDS_FAULT_RESET_1;
            faultSentOnce = true;
        }
    }
    else
    {
        switch (driveStateSP)
        {
        case EPOS_DRIVE_STATE_SWITCH_ON_DISABLED:
            controlword = EPOS_DEVICE_CONTROL_CMDS_DISABLE_VOLTAGE;
            break;
        case EPOS_DRIVE_STATE_READY_TO_SWITCH_ON:
            switch (driveState)
            {
            case EPOS_DRIVE_STATE_QUICKSTOP_ACTIVE:
                controlword = EPOS_DEVICE_CONTROL_CMDS_DISABLE_VOLTAGE;
                break;
            default:
                controlword = EPOS_DEVICE_CONTROL_CMDS_SHUTDOWN;
                break;
            }
            break;
        case EPOS_DRIVE_STATE_SWITCHED_ON:
            switch (driveState)
            {
            case EPOS_DRIVE_STATE_SWITCH_ON_DISABLED:
                controlword = EPOS_DEVICE_CONTROL_CMDS_SHUTDOWN;
                break;
            case EPOS_DRIVE_STATE_OPERATION_ENABLE:
                controlword = EPOS_DEVICE_CONTROL_CMDS_DISABLE_OPERATION;
                break;
            case EPOS_DRIVE_STATE_QUICKSTOP_ACTIVE:
                controlword = EPOS_DEVICE_CONTROL_CMDS_DISABLE_VOLTAGE;
                break;
            default: // EPOS_DRIVE_STATE_READY_TO_SWITCH_ON
                controlword = EPOS_DEVICE_CONTROL_CMDS_SWITCH_ON;
                break;
            }
            break;
        case EPOS_DRIVE_STATE_OPERATION_ENABLE:
            switch (driveState)
            {
            case EPOS_DRIVE_STATE_SWITCH_ON_DISABLED:
                controlword = EPOS_DEVICE_CONTROL_CMDS_SHUTDOWN;
                break;
            case EPOS_DRIVE_STATE_READY_TO_SWITCH_ON:
                controlword = EPOS_DEVICE_CONTROL_CMDS_SWITCH_ON;
                break;
            default:
                controlword = EPOS_DEVICE_CONTROL_CMDS_ENABLE_OPERATION;
                break;
            }
            break;
        case EPOS_DRIVE_STATE_QUICKSTOP_ACTIVE:
            switch (driveState)
            {
            case EPOS_DRIVE_STATE_SWITCH_ON_DISABLED:
                controlword = EPOS_DEVICE_CONTROL_CMDS_SHUTDOWN;
                break;
            case EPOS_DRIVE_STATE_READY_TO_SWITCH_ON:
                controlword = EPOS_DEVICE_CONTROL_CMDS_SWITCH_ON;
                break;
            case EPOS_DRIVE_STATE_SWITCHED_ON:
                controlword = EPOS_DEVICE_CONTROL_CMDS_ENABLE_OPERATION;
                break;
            default: // EPOS_DRIVE_STATE_OPERATION_ENABLE
                controlword = EPOS_DEVICE_CONTROL_CMDS_QUICKSTOP;
                break;
            }
            break;
        default:
            break;
        }

        if (controlword == EPOS_DEVICE_CONTROL_CMDS_ENABLE_OPERATION || driveState == EPOS_DRIVE_STATE_OPERATION_ENABLE)
        {
            if (operationModeSP == EPOS_MODES_OPERATION_HOMING_MODE)
            {
                if (homeSentOnce)
                    controlword += 0x10;
                else
                    homeSentOnce = true;
            }
            else
            {
                homeSentOnce = false;
                if (operationModeSP == EPOS_MODES_OPERATION_PROFILE_POSITION_MODE)
                    controlword += 0x30; // see EPOS datasheet: 0x30 = 00110000 => Interrupt actual positioning and start next positioning / Target Position is an absolute value
            }
        }
        if (driveState != EPOS_DRIVE_STATE_OPERATION_ENABLE)
            homeSentOnce = false;
    }

    return controlword;
}


bool epos::Epos::DeviceConfig()
{
    // This class emulates a CANopen device with ID classNodeID. It also
    // produces the heartbeat that is consumed by the associated EPOS with
    // ID deviceNodeID. Equal ID's might produce unpredictable behavior.
    // Therefore, if they are equal, we assume that the device configuration
    // has failed to prevent further problems.
    if (deviceNodeID == classNodeID)
    {
        std::cerr << "[ERROR]: EPOS device ID " << deviceNodeID <<
                     " is equal to this EPOS controller class ID " << classNodeID << std::endl;
        return false;
    }

    LoadSDOs();

    CANopenSDO sdo;

    // Heartbeat Producer
    sdo.dataSize = 2;
    sdo.index = CAN_OPEN_INDEX_HEARTBEAT_PRODUCER;
    sdo.subIndex = CAN_OPEN_SUBINDEX0;
    *(unsigned short*)(&(sdo.data)) = CAN_OPEN_HEARTBEAT_PRODUCER_TIME;
    deviceConfigFrames.push_back(sdo);
    sdo.ResetData();

    // Heartbeat Consumer
    sdo.dataSize = 4;
    sdo.index = CAN_OPEN_INDEX_HEARTBEAT_CONSUMER;
    sdo.subIndex = CAN_OPEN_SUBINDEX1;
    *(unsigned short*)(&(sdo.data)) = CAN_OPEN_HEARTBEAT_CONSUMER_TIME;
    sdo.data[2] = classNodeID;
    sdo.data[3] = 0;
    deviceConfigFrames.push_back(sdo);
    sdo.ResetData();

    // Receive PDO 1
    if (rPDO[0])
    {
        // Receive PDO 1 Parameter
        sdo.index = CAN_OPEN_INDEX_RECEIVE_PDO1_PARAMETER;
        {
            // COB-ID Receive PDO 1
            sdo.subIndex = CAN_OPEN_SUBINDEX1;
            sdo.dataSize = 4;
            *(unsigned short*)(&(sdo.data)) = CAN_OPEN_COB_ID_RPDO1 + deviceNodeID; // it will send the pdo with its own CAN node id (11 bits max)
            sdo.data[2] = 0; // reserved
            sdo.data[3] = 0x00; // 0 for valid (bit 31), 0 for RTR (bit 30), rest is 0
            deviceConfigFrames.push_back(sdo);
            sdo.ResetData();

            // Transmission Type Receive PDO 1
            sdo.subIndex = CAN_OPEN_SUBINDEX2;
            sdo.dataSize = 1;
            sdo.data[0] = 1; // synchronous
            deviceConfigFrames.push_back(sdo);
            sdo.ResetData();
        }

        // Receive PDO 1 Mapping
        sdo.index = CAN_OPEN_INDEX_RECEIVE_PDO1_MAPPING;
        {
            // Number of mapped Application Objects in Receive PDO (start mapping)
            sdo.subIndex = CAN_OPEN_SUBINDEX0;
            sdo.dataSize = 1;
            sdo.data[0] = 0; // PDO disabled
            deviceConfigFrames.push_back(sdo);
            sdo.ResetData();

            sdo.dataSize = 4;
            {
                // 1st mapped Object
                sdo.subIndex = CAN_OPEN_SUBINDEX1;
                *(unsigned int*)(&(sdo.data)) = EPOS_CONFIG_PDO_OBJ_CONTROLWORD;
                deviceConfigFrames.push_back(sdo);

                // 2nd mapped Object
                sdo.subIndex = CAN_OPEN_SUBINDEX2;
                *(unsigned int*)(&(sdo.data)) = EPOS_CONFIG_PDO_OBJ_MODES_OF_OPERATION;
                deviceConfigFrames.push_back(sdo);

                // 3rd mapped Object
                sdo.subIndex = CAN_OPEN_SUBINDEX3;
                *(unsigned int*)(&(sdo.data)) = EPOS_CONFIG_PDO_OBJ_DIGITAL_OUTPUT_FUNCTIONALITIES;
                deviceConfigFrames.push_back(sdo);
            }
            sdo.ResetData(); // no need to reset everytime to 0 since they all are 4 bytes

            // Number of mapped Application Objects in Receive PDO (finishing mapping)
            sdo.subIndex = CAN_OPEN_SUBINDEX0;
            sdo.dataSize = 1;
            sdo.data[0] = 3; // PDO enabled / 3 mapped objects
            deviceConfigFrames.push_back(sdo);
            sdo.ResetData();

            frameRPDO[0].can_dlc = 5; // since I configured the objects, I know the size I should send the pdo
                                      // originally was 7, it differs from the EPOS firmware manual
        }
    }

    // Receive PDO 2
    if (rPDO[1])
    {
        // Receive PDO 2 Parameter
        sdo.index = CAN_OPEN_INDEX_RECEIVE_PDO2_PARAMETER;
        {
            // COB-ID Receive PDO 2
            sdo.subIndex = CAN_OPEN_SUBINDEX1;
            sdo.dataSize = 4;
            *(unsigned short*)(&(sdo.data)) = CAN_OPEN_COB_ID_RPDO2 + deviceNodeID; // it will send the pdo with its own CAN node id (11 bits max)
            sdo.data[2] = 0; // reserved
            sdo.data[3] = 0x00; // 0 for valid (bit 31), 0 for RTR (bit 30), rest is 0
            deviceConfigFrames.push_back(sdo);
            sdo.ResetData();

            // Transmission Type Receive PDO 2
            sdo.subIndex = CAN_OPEN_SUBINDEX2;
            sdo.dataSize = 1;
            sdo.data[0] = 1; // synchronous
            deviceConfigFrames.push_back(sdo);
            sdo.ResetData();
        }

        // Receive PDO 2 Mapping
        sdo.index = CAN_OPEN_INDEX_RECEIVE_PDO2_MAPPING;
        {
            // Number of mapped Application Objects in Receive PDO (start mapping)
            sdo.subIndex = CAN_OPEN_SUBINDEX0;
            sdo.dataSize = 1;
            sdo.data[0] = 0; // PDO disabled
            deviceConfigFrames.push_back(sdo);
            sdo.ResetData();

            sdo.dataSize = 4;
            {
                // 1th mapped Object
                sdo.subIndex = CAN_OPEN_SUBINDEX1;
                *(unsigned int*)(&(sdo.data)) = EPOS_CONFIG_PDO_OBJ_CURRENT_MODE_SETTING_VALUE;
                deviceConfigFrames.push_back(sdo);

                // 2nd mapped Object
                sdo.subIndex = CAN_OPEN_SUBINDEX2;
                *(unsigned int*)(&(sdo.data)) = EPOS_CONFIG_PDO_OBJ_TARGET_VELOCITY;
                deviceConfigFrames.push_back(sdo);
            }
            sdo.ResetData(); // no need to reset everytime to 0 since they all are 4 bytes

            // Number of mapped Application Objects in Receive PDO (finishing mapping)
            sdo.subIndex = CAN_OPEN_SUBINDEX0;
            sdo.dataSize = 1;
            sdo.data[0] = 2; // PDO enabled / 2 mapped objects
            deviceConfigFrames.push_back(sdo);
            sdo.ResetData();

            frameRPDO[1].can_dlc = 6; // since I configured the objects, I know the size I should send the pdo
                                      // originally was 8, it differs from the EPOS firmware manual
        }
    }

    // Receive PDO 3
    if (rPDO[2])
    {
        // Receive PDO 3 Parameter
        sdo.index = CAN_OPEN_INDEX_RECEIVE_PDO3_PARAMETER;
        {
            // COB-ID Receive PDO 3
            sdo.subIndex = CAN_OPEN_SUBINDEX1;
            sdo.dataSize = 4;
            *(unsigned short*)(&(sdo.data)) = CAN_OPEN_COB_ID_RPDO3 + deviceNodeID; // it will send the pdo with its own CAN node id (11 bits max)
            sdo.data[2] = 0; // reserved
            sdo.data[3] = 0x00; // 0 for valid (bit 31), 0 for RTR (bit 30), rest is 0
            deviceConfigFrames.push_back(sdo);
            sdo.ResetData();

            // Transmission Type Receive PDO 3
            sdo.subIndex = CAN_OPEN_SUBINDEX2;
            sdo.dataSize = 1;
            sdo.data[0] = 1; // synchronous
            deviceConfigFrames.push_back(sdo);
            sdo.ResetData();
        }

        // Receive PDO 3 Mapping
        sdo.index = CAN_OPEN_INDEX_RECEIVE_PDO3_MAPPING;
        {
            // Number of mapped Application Objects in Receive PDO (start mapping)
            sdo.subIndex = CAN_OPEN_SUBINDEX0;
            sdo.dataSize = 1;
            sdo.data[0] = 0; // PDO disabled
            deviceConfigFrames.push_back(sdo);
            sdo.ResetData();

            sdo.dataSize = 4;
            {
                // 1th mapped Object
                sdo.subIndex = CAN_OPEN_SUBINDEX1;
                *(unsigned int*)(&(sdo.data)) = EPOS_CONFIG_PDO_OBJ_TARGET_POSITION;
                deviceConfigFrames.push_back(sdo);
            }
            sdo.ResetData(); // no need to reset everytime to 0 since they all are 4 bytes

            // Number of mapped Application Objects in Receive PDO (finishing mapping)
            sdo.subIndex = CAN_OPEN_SUBINDEX0;
            sdo.dataSize = 1;
            sdo.data[0] = 1; // PDO enabled / 1 mapped objects
            deviceConfigFrames.push_back(sdo);
            sdo.ResetData();

            frameRPDO[2].can_dlc = 4; // since I configured the objects, I know the size I should send the pdo
        }
    }

    // Transmit PDO 1
    if (tPDO[0])
    {
        // Transmit PDO 1 Parameter
        sdo.index = CAN_OPEN_INDEX_TRANSMIT_PDO1_PARAMTER;
        {
            // COB-ID Transmit PDO 1
            sdo.subIndex = CAN_OPEN_SUBINDEX1;
            sdo.dataSize = 4;
            *(unsigned short*)(&(sdo.data)) = CAN_OPEN_COB_ID_TPDO1 + deviceNodeID; // it will send the pdo with its own CAN node id (11 bits max)
            sdo.data[2] = 0; // reserved
            sdo.data[3] = 0x00; // 0 for valid (bit 31), 0 for RTR (bit 30), rest is 0
            deviceConfigFrames.push_back(sdo);
            sdo.ResetData();

            // Transmission Type Transmit PDO 1
            sdo.subIndex = CAN_OPEN_SUBINDEX2;
            sdo.dataSize = 1;
            sdo.data[0] = 1; // synchronous
            deviceConfigFrames.push_back(sdo);
            sdo.ResetData();

            // Inhibit Time Transmit PDO 1
            sdo.subIndex = CAN_OPEN_SUBINDEX3;
            sdo.dataSize = 2;
            *(unsigned short*)(&(sdo.data)) = 0; // 0 ms for event triggered PDO (i think its for Acyclic)
            deviceConfigFrames.push_back(sdo);
            sdo.ResetData();
        }

        // Transmit PDO 1 Mapping
        sdo.index = CAN_OPEN_INDEX_TRANSMIT_PDO1_MAPPING;
        {
            // Number of mapped Application Objects in Transmit PDO (start mapping)
            sdo.subIndex = CAN_OPEN_SUBINDEX0;
            sdo.dataSize = 1;
            sdo.data[0] = 0; // PDO disabled
            deviceConfigFrames.push_back(sdo);
            sdo.ResetData();

            sdo.dataSize = 4;
            {
                // 1st mapped Object
                sdo.subIndex = CAN_OPEN_SUBINDEX1;
                *(unsigned int*)(&(sdo.data)) = EPOS_CONFIG_PDO_OBJ_STATUSWORD;
                deviceConfigFrames.push_back(sdo);

                // 2st mapped Object
                sdo.subIndex = CAN_OPEN_SUBINDEX2;
                *(unsigned int*)(&(sdo.data)) = EPOS_CONFIG_PDO_OBJ_MODES_OF_OPERATION_DISPLAY;
                deviceConfigFrames.push_back(sdo);

                // 3rd mapped Object
                sdo.subIndex = CAN_OPEN_SUBINDEX3;
                *(unsigned int*)(&(sdo.data)) = EPOS_CONFIG_PDO_OBJ_CURRENT_ACTUAL_VALUE;
                deviceConfigFrames.push_back(sdo);
            }
            sdo.ResetData(); // no need to reset everytime to 0 since they all are 4 bytes

            // Number of mapped Application Objects in Transmit PDO (finishing mapping)
            sdo.subIndex = CAN_OPEN_SUBINDEX0;
            sdo.dataSize = 1;
            sdo.data[0] = 3; // PDO enabled / 3 mapped objects
            deviceConfigFrames.push_back(sdo);
            sdo.ResetData();
        }
    }

    // Transmit PDO 2
    if (tPDO[1])
    {
        // Transmit PDO 2 Parameter
        sdo.index = CAN_OPEN_INDEX_TRANSMIT_PDO2_PARAMTER;
        {
            // COB-ID Transmit PDO 2
            sdo.subIndex = CAN_OPEN_SUBINDEX1;
            sdo.dataSize = 4;
            *(unsigned short*)(&(sdo.data)) = CAN_OPEN_COB_ID_TPDO2 + deviceNodeID; // it will send the pdo with its own CAN node id (11 bits max)
            sdo.data[2] = 0; // reserved
            sdo.data[3] = 0x00; // 0 for valid (bit 31), 0 for RTR (bit 30), rest is 0
            deviceConfigFrames.push_back(sdo);
            sdo.ResetData();

            // Transmission Type Transmit PDO 2
            sdo.subIndex = CAN_OPEN_SUBINDEX2;
            sdo.dataSize = 1;
            sdo.data[0] = 1; // synchronous
            deviceConfigFrames.push_back(sdo);
            sdo.ResetData();

            // Inhibit Time Transmit PDO 2
            sdo.subIndex = CAN_OPEN_SUBINDEX3;
            sdo.dataSize = 2;
            *(unsigned short*)(&(sdo.data)) = 0; // 0 ms for event triggered PDO (i think its for Acyclic)
            deviceConfigFrames.push_back(sdo);
            sdo.ResetData();
        }

        // Transmit PDO 2 Mapping
        sdo.index = CAN_OPEN_INDEX_TRANSMIT_PDO2_MAPPING;
        {
            // Number of mapped Application Objects in Transmit PDO (start mapping)
            sdo.subIndex = CAN_OPEN_SUBINDEX0;
            sdo.dataSize = 1;
            sdo.data[0] = 0; // PDO disabled
            deviceConfigFrames.push_back(sdo);
            sdo.ResetData();

            sdo.dataSize = 4;
            {
                // 1st mapped Object
                sdo.subIndex = CAN_OPEN_SUBINDEX1;
                *(unsigned int*)(&(sdo.data)) = EPOS_CONFIG_PDO_OBJ_POSITION_ACTUAL_VALUE;
                deviceConfigFrames.push_back(sdo);

                // 2nd mapped Object
                sdo.subIndex = CAN_OPEN_SUBINDEX2;
                *(unsigned int*)(&(sdo.data)) = EPOS_CONFIG_PDO_OBJ_VELOCITY_ACTUAL_VALUE_AVERAGED;
                deviceConfigFrames.push_back(sdo);
            }
            sdo.ResetData(); // no need to reset everytime to 0 since they all are 4 bytes

            // Number of mapped Application Objects in Transmit PDO (finishing mapping)
            sdo.subIndex = CAN_OPEN_SUBINDEX0;
            sdo.dataSize = 1;
            sdo.data[0] = 2; // PDO enabled / 2 mapped objects
            deviceConfigFrames.push_back(sdo);
            sdo.ResetData();
        }
    }

    // Transmit PDO 3
    if (tPDO[2])
    {
        // Transmit PDO 3 Parameter
        sdo.index = CAN_OPEN_INDEX_TRANSMIT_PDO3_PARAMTER;
        {
            // COB-ID Transmit PDO 3
            sdo.subIndex = CAN_OPEN_SUBINDEX1;
            sdo.dataSize = 4;
            *(unsigned short*)(&(sdo.data)) = CAN_OPEN_COB_ID_TPDO3 + deviceNodeID; // it will send the pdo with its own CAN node id (11 bits max)
            sdo.data[2] = 0; // reserved
            sdo.data[3] = 0x00; // 0 for valid (bit 31), 0 for RTR (bit 30), rest is 0
            deviceConfigFrames.push_back(sdo);
            sdo.ResetData();

            // Transmission Type Transmit PDO 3
            sdo.subIndex = CAN_OPEN_SUBINDEX2;
            sdo.dataSize = 1;
            sdo.data[0] = 1; // synchronous
            deviceConfigFrames.push_back(sdo);
            sdo.ResetData();

            // Inhibit Time Transmit PDO 3
            sdo.subIndex = CAN_OPEN_SUBINDEX3;
            sdo.dataSize = 2;
            *(unsigned short*)(&(sdo.data)) = 0; // 0 ms for event triggered PDO (i think its for Acyclic)
            deviceConfigFrames.push_back(sdo);
            sdo.ResetData();
        }

        // Transmit PDO 3 Mapping
        sdo.index = CAN_OPEN_INDEX_TRANSMIT_PDO3_MAPPING;
        {
            // Number of mapped Application Objects in Transmit PDO (start mapping)
            sdo.subIndex = CAN_OPEN_SUBINDEX0;
            sdo.dataSize = 1;
            sdo.data[0] = 0; // PDO disabled
            deviceConfigFrames.push_back(sdo);
            sdo.ResetData();

            sdo.dataSize = 4;
            {
                // 1st mapped Object
                sdo.subIndex = CAN_OPEN_SUBINDEX1;
                *(unsigned int*)(&(sdo.data)) = EPOS_CONFIG_PDO_OBJ_ANALOG_INPUT_1;
                deviceConfigFrames.push_back(sdo);

                // 2nd mapped Object
                sdo.subIndex = CAN_OPEN_SUBINDEX2;
                *(unsigned int*)(&(sdo.data)) = EPOS_CONFIG_PDO_OBJ_ANALOG_INPUT_2;
                deviceConfigFrames.push_back(sdo);

                // 3rd mapped Object
                sdo.subIndex = CAN_OPEN_SUBINDEX3;
                *(unsigned int*)(&(sdo.data)) = EPOS_CONFIG_PDO_OBJ_DIGITAL_INPUT_FUNCTIONALITIES_STATE;
                deviceConfigFrames.push_back(sdo);
            }
            sdo.ResetData(); // no need to reset everytime to 0 since they all are 4 bytes

            // Number of mapped Application Objects in Transmit PDO (finishing mapping)
            sdo.subIndex = CAN_OPEN_SUBINDEX0;
            sdo.dataSize = 1;
            sdo.data[0] = 3; // PDO enabled / 3 mapped objects
            deviceConfigFrames.push_back(sdo);
            sdo.ResetData();
        }
    }

    return CANopen::DeviceConfig();

    deviceConfigFrames.clear();
}


void epos::Epos::SendingPDO()
{
    mutUpdate.lock();

    if (rPDO[0])
    {
        *(unsigned short*)(&(frameRPDO[0].data[0])) = GenerateControlWord();

        frameRPDO[0].data[2] = operationModeSP;

        frameRPDO[0].data[3] = 0;
        frameRPDO[0].data[4] = 0;
        for (unsigned char i = 0; i < EPOS_MAX_DIGITAL_OUTPUTS; i++)
        {
            if (digitalOutput[i])
                frameRPDO[0].data[4] += 1 << (EPOS_MAX_DIGITAL_OUTPUTS - 1 - i);
        }
    }

    if (rPDO[1])
    {
        *(short*)(&(frameRPDO[1].data[0])) = currentSP*gainInversion;
        *(int*)(&(frameRPDO[1].data[2])) = (int)(velocitySP*gainVelocity);
    }

    if (rPDO[2])
        *(int*)(&(frameRPDO[2].data[0])) = (int)(positionSP*gainPosition);

    mutUpdate.unlock();
}


void epos::Epos::ReceivingPDO()
{
    if (tPDO[0] && tPDOReceived[0]) // Receiving Transmit PDO 1
    {
        unsigned short statusword = *(unsigned short*)(&(frameTPDO[0].data[0]));
        driveState = statusword&0x417F;
        targetReached = statusword&0x400;
        homingAttained = statusword&0x1000;
        homingError = statusword&0x2000;
        operationMode = frameTPDO[0].data[2];
        current = *(short*)(&(frameTPDO[0].data[3]))*gainInversion;
    }

    if (tPDO[1] && tPDOReceived[1]) // Receiving Transmit PDO 2
    {
        position = ((double)(*(int*)(&(frameTPDO[1].data[0]))))*gainPositionInverted;
        velocity = ((double)(*(int*)(&(frameTPDO[1].data[4]))))*gainVeocityInverted;
    }

    if (tPDO[2] && tPDOReceived[2]) // Receiving Transmit PDO 3
    {
        analogInput[0] = *(short*)(&(frameTPDO[2].data[0]));
        analogInput[1] = *(short*)(&(frameTPDO[2].data[2]));
        for (unsigned char i = 0; i < EPOS_MAX_DIGITAL_INPUTS; i++)
        {
            short temp = 1 << (EPOS_MAX_DIGITAL_INPUTS - 1 - i);
            digitalInput[0] = ((*(short*)(&(frameTPDO[2].data[4])) & temp) == temp);
        }
    }
}


void epos::Epos::Emergency(const can_frame &frame) const
{
    using namespace std;

    cerr << "[ERROR] EMERGENCY id (";
    PrintHex(cerr, frame.can_id);
    cerr << ") code (0x";
    PrintHex(cerr, frame.data[1]);
    PrintHex(cerr, frame.data[0]);
    cerr << ") register (";
    PrintBin<8>(cerr, frame.data[2]);
    cerr << "b)" << endl;
}


epos::Epos::~Epos()
{
    tryToReconnect = false;
    Disconnect(true);
}


// can_open_sdo.cpp



#include <epos/can_open_sdo.h>



epos::CANopenSDO & epos::CANopenSDO::operator=(const CANopenSDO& sdo)
{
    dataSize = sdo.dataSize;
    index = sdo.index;
    subIndex = sdo.subIndex;
    for (unsigned int i = 0; i < dataSize; i++)
        data[i] = sdo.data[i];
    return *this;
}


void epos::CANopenSDO::ResetData()
{
    for (unsigned char i = 0; i < 4; i++)
        data[i] = 0;
}

// epos_nodelet.cpp



#include <epos/epos_nodelet.h>



epos::EposNodelet::EposNodelet(): TimingLog("/logs/epos_nodelet")
{
    initialized = false;
}


void epos::EposNodelet::onInit()
{
    ros::NodeHandle & privateNH = getPrivateNodeHandle();

    int iTemp;
    privateNH.param("can_device", canDevice, canDevice);
    privateNH.param("device_id", iTemp, 0);
    deviceNodeID = (iTemp > 0 && iTemp < 255) ? (unsigned int)iTemp : 1;
    privateNH.param("personal_id", iTemp, 0);
    classNodeID = (iTemp > 0 && iTemp < 255) ? (unsigned int)iTemp : 1;
    double dTemp;
    privateNH.param("gain_position", dTemp, (double)1);
    gainPosition = (dTemp >= 0) ? dTemp : (double)1;
    privateNH.param("gain_velocity", dTemp, (double)1);
    gainVelocity = (dTemp >= 0) ? dTemp : (double)1;
    privateNH.param("inverted", inverted, false);

    if (inverted)
    {
        gainInversion = -1;
        gainPosition = -gainPosition;
        gainVelocity = -gainVelocity;
    }
    gainPositionInverted = 1/gainPosition;
    gainVeocityInverted = 1/gainVelocity;

    ros::NodeHandle nodehandle = getNodeHandle();

    MountFrames();

    msgEposStatePub = nodehandle.advertise<std_msgs::UInt8>(getName() + "/state", 10, true);
    msgEposInfoPub = nodehandle.advertise<epos::Info>(getName() + "/info", 10, true);
    msgEposEmergencyPub = nodehandle.advertise<std_msgs::ByteMultiArray>(getName() + "/emergency", 10, true);
    msgOutputPub = nodehandle.advertise<std_msgs::Float64>(getName() + "/output", 10, true);

    srvEposControlSer = nodehandle.advertiseService(getName() + "/control", &EposNodelet::ControlCallback, this);
    srvEposDAOutputsSer = nodehandle.advertiseService(getName() + "/d_a_outputs", &EposNodelet::DAOutputsCallback, this);
    srvEposConnectSer = nodehandle.advertiseService(getName() + "/connect", &EposNodelet::ConnectCallback, this);

    initialized = true;

    SetState(CanOpenState::NotConnected);

    Connect();

    std::string str = getName();
    setFileName("/logs/" + str + "/");
}


void epos::EposNodelet::SetState(unsigned char state)
{
    Epos::SetState(state);

    if (initialized)
    {
        std_msgs::UInt8 msg;
        msg.data = state;
        msgEposStatePub.publish(msg);
    }
}


void epos::EposNodelet::LoadSDOs()
{
    ros::NodeHandle & privateNH = getPrivateNodeHandle();

    std::vector<std::string> allParametersNames, sdoNames;
    privateNH.getParamNames(allParametersNames);
    std::string ns = privateNH.getNamespace() + "/sdos";
    for (unsigned int i = 0; i < allParametersNames.size(); i++)
    {
        std::size_t pos = allParametersNames[i].find(ns);
        if (pos != std::string::npos)
        {
            std::string filteredName = allParametersNames[i].substr(pos + ns.size());
            std::size_t pos2 = filteredName.find("/", 1);
            if (pos2 != std::string::npos)
            {
                std::string sdoName = filteredName.substr(1, pos2);
                if (std::find(sdoNames.begin(), sdoNames.end(), sdoName) == sdoNames.end())
                    sdoNames.push_back(sdoName);
            }
        }
    }

    CANopenSDO sdo;
    for (unsigned int i = 0; i < sdoNames.size(); i++)
    {
        std::string sdoName = sdoNames[i];
        if (privateNH.hasParam("sdos/" + sdoName + "/index")
                && privateNH.hasParam("sdos/" + sdoName + "/sub_index")
                && privateNH.hasParam("sdos/" + sdoName + "/data_type")
                && privateNH.hasParam("sdos/" + sdoName + "/data"))
        {
            std::string dataType;
            privateNH.getParam("sdos/" + sdoName + "/data_type", dataType);
            bool dataTypeOk = true;
            int data = 0;
            privateNH.getParam("sdos/" + sdoName + "/data", data);
            if (dataType == "INTEGER8")
            {
                *(char*)(&(sdo.data[0])) = (char)data;
                sdo.dataSize = 1;
            }
            else if (dataType == "UNSIGNED8")
            {
                *(unsigned char*)(&(sdo.data[0])) = (unsigned char)data;
                sdo.dataSize = 1;
            }
            else if (dataType == "INTEGER16")
            {
                *(short*)(&(sdo.data[0])) = (short)data;
                sdo.dataSize = 2;
            }
            else if (dataType == "UNSIGNED16")
            {
                *(unsigned short*)(&(sdo.data[0])) = (unsigned short)data;
                sdo.dataSize = 2;
            }
            else if (dataType == "INTEGER32")
            {
                *(int*)(&(sdo.data[0])) = (int)data;
                sdo.dataSize = 4;
            }
            else if (dataType == "UNSIGNED32")
            {
                *(unsigned int*)(&(sdo.data[0])) = (unsigned int)data;
                sdo.dataSize = 4;
            }
            else
                dataTypeOk = false;

            if (dataTypeOk)
            {
                int temp;
                privateNH.getParam("sdos/" + sdoName + "/index", temp);
                sdo.index = (unsigned short)temp;
                privateNH.getParam("sdos/" + sdoName + "/sub_index", temp);
                sdo.subIndex = (unsigned char)temp;

                deviceConfigFrames.push_back(sdo);
            }
        }
    }
}


void epos::EposNodelet::ReceivingPDO()
{
    Epos::ReceivingPDO();

    std::stringstream stream;
    stream << "pos = " << position << " | vel = " << velocity << " | current = " << current;
    addToLog(stream.str(), "pdo_received");

    epos::InfoPtr msg(new epos::Info);
    msg->current = current;
    msg->position = position;
    msg->velocity = velocity;

    if (driveState == EPOS_DRIVE_STATE_OPERATION_ENABLE)
    {
        // BUG: I DON'T KNOW HOW TO GET THE ACTUAL SET POINT VALUE USED, SO I'M GOING TO USE THE LOCAL VALUE (THIS IS WRONG, IT WOULD PROBABLY BE NECESSARY MORE T PDO TO KNOW THESE VALUES)
        switch (operationMode)
        {
        case EPOS_MODES_OPERATION_HOMING_MODE:
            msg->controlMode = ControlType::Homing;
            msg->controlSetPoint = 0;
            break;
        case EPOS_MODES_OPERATION_PROFILE_POSITION_MODE:
            msg->controlMode = ControlType::Position;
            msg->controlSetPoint = positionSP;
            break;
        case EPOS_MODES_OPERATION_PROFILE_VELOCITY_MODE:
            msg->controlMode = ControlType::Velocity;
            msg->controlSetPoint = velocitySP;
            break;
        case EPOS_MODES_OPERATION_CURRENT_MODE:
            msg->controlMode = ControlType::Current;
            msg->controlSetPoint = currentSP;
            break;
        default:
            msg->controlMode = ControlType::NotControlled;
            msg->controlSetPoint = 0;
            break;
        }

        if (operationMode == EPOS_MODES_OPERATION_PROFILE_POSITION_MODE
                || operationMode == EPOS_MODES_OPERATION_PROFILE_VELOCITY_MODE
                || operationMode == EPOS_MODES_OPERATION_HOMING_MODE)
            msg->targetReached = targetReached;
        else
            msg->targetReached = false;

        if (operationMode == EPOS_MODES_OPERATION_HOMING_MODE)
        {
            msg->homingAttained = homingAttained;
            msg->homingError = homingError;
        }
        else
        {
            msg->homingAttained = false;
            msg->homingError = false;
        }
    }
    else if (driveState == EPOS_DRIVE_STATE_SWITCHED_ON)
    {
        msg->controlMode = ControlType::SwitchedOn;
        msg->controlSetPoint = 0;
    }
    else
    {
        msg->controlMode = ControlType::NotControlled;
        msg->controlSetPoint = 0;
    }

    unsigned int i;

    for (i = 0; i < EPOS_MAX_ANALOG_INPUTS; i++)
            msg->analogInputs[i]= analogInput[i];

    for (i = 0; i < EPOS_MAX_DIGITAL_INPUTS; i++)
            msg->digitalInputs[i] = digitalInput[i];

    for (i = 0; i < EPOS_MAX_ANALOG_OUTPUTS; i++)
            msg->analogOutputs[i] = analogOutput[i];

    for (i = 0; i < EPOS_MAX_DIGITAL_OUTPUTS; i++)
            msg->digitalOutputs[i] = digitalOutput[i];

    msgEposInfoPub.publish(msg);
}


bool epos::EposNodelet::ControlCallback(epos::Control::Request & req, epos::Control::Response & res)
{
    mutUpdate.lock();

    addToLog(std::to_string(req.data), "control_epos_nodelet");
    res.ok = true;

    // If these SDOs must be sent, the NMT state must be the Pre-Operacional because PDOs can only be configured in that state
    // The SendNMT may return false because there is no wait time (time for an answer to be acquired of the NMT state change).
    // The code implemented just believe that the NMT will be changed.

    if (res.ok)
    {
        std_msgs::Float64 msgOutput;
        // Even if it's not in the Operation Enable, the desired operation mode and value will be updated
        switch (req.mode)
        {
        case ControlType::SwitchedOn:
            driveStateSP = EPOS_DRIVE_STATE_SWITCHED_ON;
            rPDO[1] = true;
            break;
        case ControlType::Homing:
            driveStateSP = EPOS_DRIVE_STATE_OPERATION_ENABLE;
            operationModeSP = EPOS_MODES_OPERATION_HOMING_MODE;
            rPDO[1] = true;
            break;
        case ControlType::Current:
            driveStateSP = EPOS_DRIVE_STATE_OPERATION_ENABLE;
            operationModeSP = EPOS_MODES_OPERATION_CURRENT_MODE;
            currentSP = (short)req.data;
            msgOutput.data = currentSP*gainInversion;
            rPDO[1] = true;
            break;
        case ControlType::Velocity:
            driveStateSP = EPOS_DRIVE_STATE_OPERATION_ENABLE;
            operationModeSP = EPOS_MODES_OPERATION_PROFILE_VELOCITY_MODE;
            velocitySP = req.data;
            msgOutput.data = velocitySP*gainVelocity;
            rPDO[1] = true;
            break;
        case ControlType::Position:
            driveStateSP = EPOS_DRIVE_STATE_OPERATION_ENABLE;
            operationModeSP = EPOS_MODES_OPERATION_PROFILE_POSITION_MODE;
            positionSP = req.data;
            msgOutput.data = positionSP*gainPosition;
            rPDO[1] = true;
            break;
        case ControlType::NotControlled:
            driveStateSP = EPOS_DRIVE_STATE_READY_TO_SWITCH_ON;
            rPDO[1] = true;
        default:
            res.ok = false;
            break;
        }
        msgOutputPub.publish(msgOutput);

        if (driveState != EPOS_DRIVE_STATE_OPERATION_ENABLE)
            res.ok = false;
    }

    mutUpdate.unlock();

    return true;
}


bool epos::EposNodelet::DAOutputsCallback(epos::DAOutputs::Request & req, epos::DAOutputs::Response & res)
{
    mutUpdate.lock();

    unsigned char i;
    epos::NumberValueInt valueInt;
    epos::NumberValueBool valueBool;

    for (i = 0; i < req.analogOutputs.size(); i++)
    {
        valueInt = req.analogOutputs[i];
        if (valueInt.number < EPOS_MAX_ANALOG_OUTPUTS)
           analogOutput[valueInt.number] = valueInt.value;
    }
    for (i = 0; i < req.digitalOutputs.size(); i++)
    {
        valueBool = req.digitalOutputs[i];
        if (valueBool.number < EPOS_MAX_DIGITAL_OUTPUTS)
            digitalOutput[valueBool.number] = valueBool.value;
    }

    res.ok = true;

    mutUpdate.unlock();

    return true;
}


bool epos::EposNodelet::ConnectCallback(epos::Connect::Request & req, epos::Connect::Response & res)
{
    if (req.connect)
    {
        tryToReconnect = true;
        Connect();
    }
    else
    {
        tryToReconnect = false;
        Disconnect();
    }
    return true;
}


void epos::EposNodelet::Emergency(const can_frame &frame) const
{
    Epos::Emergency(frame);

    std_msgs::ByteMultiArrayPtr msg(new std_msgs::ByteMultiArray);
    for (unsigned char i = 0; i < frame.can_dlc; i++)
        msg->data.push_back(frame.data[i]);
    msgEposEmergencyPub.publish(msg);
}


epos::EposNodelet::~EposNodelet()
{
}
